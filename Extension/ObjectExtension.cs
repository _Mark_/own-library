using System.Linq;

namespace Extension
{
  public static class ObjectExtension
  {
    public static T FindObjectOfType<T>(this UnityEngine.Object obj, bool includeInactive = false) where T : class
    {
      var objs = UnityEngine.Object.FindObjectsOfType<UnityEngine.Object>(includeInactive).OfType<T>().ToArray();
      if (objs.Length < 1)
        return null;

      return objs[0];
    }
    
    public static T[] FindObjectsOfType<T>(this UnityEngine.Object obj, bool includeInactive = false) where T : class =>
      UnityEngine.Object.FindObjectsOfType<UnityEngine.Object>(includeInactive).OfType<T>().ToArray();
  }
}