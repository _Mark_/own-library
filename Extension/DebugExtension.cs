using UnityEngine;

namespace Extension
{
  public static class DebugExtension
  {
    public static void DrawCross(Vector3 position, float size)
    {
      DrawCross(position, size, Color.white);
    }
    
    public static void DrawCross(Vector3 position, float size, Color color)
    {
      UnityEngine.Debug.DrawLine(position, position + Vector3.up * size, color);
      UnityEngine.Debug.DrawLine(position, position + Vector3.down * size, color);
      UnityEngine.Debug.DrawLine(position, position + Vector3.left * size, color);
      UnityEngine.Debug.DrawLine(position, position + Vector3.right * size, color);
      UnityEngine.Debug.DrawLine(position, position + Vector3.forward * size, color);
      UnityEngine.Debug.DrawLine(position, position + Vector3.back * size, color);
    }
  }
}
