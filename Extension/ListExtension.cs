using System.Collections.Generic;

namespace Extension
{
	public static class ListExtension
	{
		public static List<T> AddNext<T>(this List<T> list, T element)
		{
			list.Add(element);
			return list;
		}
	}
}