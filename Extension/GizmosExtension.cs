using UnityEngine;

namespace Extension
{
  public static class GizmosExtension
  {
    public static void DrawCross(Vector3 position, float size)
    {
      UnityEngine.Gizmos.DrawLine(position, position + Vector3.up * size);
      UnityEngine.Gizmos.DrawLine(position, position + Vector3.down * size);
      UnityEngine.Gizmos.DrawLine(position, position + Vector3.left * size);
      UnityEngine.Gizmos.DrawLine(position, position + Vector3.right * size);
      UnityEngine.Gizmos.DrawLine(position, position + Vector3.forward * size);
      UnityEngine.Gizmos.DrawLine(position, position + Vector3.back * size);
    }
  }
}
