using UnityEngine;

namespace Extension
{
	public static class Vector3Extension
	{
		public static Vector3 CounterclockwiseRotate(this Vector3 vector, float angle, Vector3 axis)
		{
			return Quaternion.AngleAxis(angle, axis) * vector;
		}
		
		public static Vector3 ClockwiseRotate(this Vector3 vector, float angle, Vector3 axis)
		{
			return Quaternion.AngleAxis(-angle, axis) * vector;
		}
	}
}