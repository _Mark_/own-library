﻿using System;

namespace Mechanics.HealthSystem
{
	public sealed class TakenDamageArgs : EventArgs
	{
		public int DamageValueValue;

		public TakenDamageArgs(int damageValue)
		{
			DamageValueValue = damageValue;
		}
	}
}