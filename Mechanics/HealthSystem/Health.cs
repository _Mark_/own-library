﻿using System;
using UnityEngine;

namespace Mechanics.HealthSystem
{
	public class Health : MonoBehaviour, IHealth
	{
		[SerializeField] private bool _isDead;
		[SerializeField] private int _value;
		[SerializeField] private int _min;
		[SerializeField] private int _max;

		public int Value => _value;
		public int Min => _min;
		public int Max => _max;

		public event Action Death;
		public event EventHandler<TakenHealArgs> TakenHeal;
		public event EventHandler<TakenDamageArgs> TakenDamage;

		public bool IsDead => _isDead;

		public void Reset()
		{
			_value = _max;
			_isDead = false;
		}

		public void Heal(int heal)
		{
			if (_isDead) return;

			_value += heal;
			TakenHeal?.Invoke(this, new TakenHealArgs(heal));

			if (_value > _max)
				_value = _max;
		}

		public void TakeDamage(int damage)
		{
			if (_isDead) return;

			_value -= damage;
			TakenDamage?.Invoke(this, new TakenDamageArgs(damage));

			if (_value <= _min)
			{
				OnDeath();
			}
		}

		public void Kill()
		{
			_value = _min;
			OnDeath();
		}

		private void OnDeath()
		{
			_isDead = true;
			Death?.Invoke();
		}
	}
}