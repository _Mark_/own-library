using UnityEngine;

public static class BezierCurve
	{
		private static class Vector3Calculator
		{
			public static Vector3 Point(Vector3[] points, float t)
			{
				var num = points.Length;

				switch (num)
				{
					case 0:
						Debug.LogError("Has not points!");
						return Vector3.zero;
					case 1:
						return points[0];
				}

				if (num > 2)
				{
					var newPoints = new Vector3[num - 1];
					for (int i = 0; i < newPoints.Length; i++)
					{
						newPoints[i] = Point(points[i], points[i + 1], t);
					}

					return Point(newPoints, t);
				}

				return Point(points[0], points[1], t);
			}

			public static Vector3 Point(Vector3 p1, Vector3 p2, float t) =>
				new Vector3
				{
					x = BezierCurve.Point(p1.x, p2.x, t),
					y = BezierCurve.Point(p1.y, p2.y, t),
					z = BezierCurve.Point(p1.z, p2.z, t)
				};
		}
		
		private static class Vector2Calculator
		{
			public static Vector2 Point(Vector2[] points, float t)
			{
				var num = points.Length;

				switch (num)
				{
					case 0:
						Debug.LogError("Has not points!");
						return Vector2.zero;
					case 1:
						return points[0];
				}

				if (num > 2)
				{
					var newPoints = new Vector2[num - 1];
					for (int i = 0; i < newPoints.Length; i++)
					{
						newPoints[i] = Point(points[i], points[i + 1], t);
					}

					return Point(newPoints, t);
				}

				return Point(points[0], points[1], t);
			}

			public static Vector2 Point(Vector2 p1, Vector2 p2, float t) =>
				new Vector2
				{
					x = BezierCurve.Point(p1.x, p2.x, t),
					y = BezierCurve.Point(p1.y, p2.y, t)
				};
		}

		public static Vector3 Point(Vector3[] points, float t) =>
			Vector3Calculator.Point(points, t);

		public static Vector2 Point(Vector2[] points, float t) =>
			Vector2Calculator.Point(points, t);

		public static Vector3 Point(Vector3 p1, Vector3 p2, float t) =>
			Vector3Calculator.Point(p1, p2, t);

		public static Vector2 Point(Vector2 p1, Vector2 p2, float t) =>
			Vector2Calculator.Point(p1, p2, t);

		public static float Point(float p1, float p2, float t) =>
			(1 - t) * p1 + t * p2;
	}